#ifndef _IMAGE_H
#define _IMAGE_H

#include "Pixel.h"
#include <string>

/**
 * @class Image
 * 
 * @brief Une Image est un rectangle en 2 dimensions de Pixel. Elle est caractérisée selon sa largeur (x) et sa hauteur (y).
*/

class Image {
    private:
        Pixel* tab; ///<Tableau de Pixel de taille dimx*dimy.
        int dimx; ///<Largeur de l'image.
        int dimy; ///<Hauteur de l'image.

    public:
        Image();
        Image(int widht, int height);
        ~Image();
        /**
         * @brief Renvoie un lien sur le Pixel de coordonnées (x,y).
         * 
         * @param x Abscisse du Pixel.
         * @param y Ordonnée du Pixel.
        */
        Pixel& getPix(int x, int y);
        /**
         * @brief Renvoie un lien sur une copie du Pixel de coordonnées (x,y).
         * 
         * @param x Abscisse du Pixel.
         * @param y Ordonnée du Pixel.
        */
        Pixel getPixCopie(int x, int y) const;
        /**
         * @brief Change un Pixel de l'Image à la couleur donnée.
         * 
         * @param x Abscisse du Pixel.
         * @param y Ordonnée du Pixel.
         * @param couleur Couleur avec laquelle sera coloriée le Pixel.
        */
        void setPix(int x, int y, Pixel couleur);
        /**
         * @brief Met tout les Pixel à la couleur donnée dans un rectangle plein de coins (xmin, ymin) et (xmax,ymax).
         * 
         * @param xmin Abscisse du premier coin, xmin <= xmax.
         * @param ymin Ordonnee du premier coin, ymin <= ymax.
         * @param xmax Abscisse du deuxieme coin, xmax >= xmin.
         * @param ymax Ordonnee du deuxieme coin, ymax >= ymin.
         * @param couleur Couleur avec laquelle sera coloriée le rectangle.
        */
        void dessinerRectangle(int xmin, int ymin, int xmax, int ymax, Pixel couleur);
        /** 
         * @brief Remplace toute l'Image par la même couleur de partout.
         * 
         * @param couleur Couleur avec laquelle sera coloriée l'Image.
        */
        void effacer(Pixel couleur);
        /**
         * @brief Sauvegarde l'Image dans un fichier de nom filemane.
         * 
         * @param filename Nom du fichier où est sauvée l'Image.
        */
        void sauver(const std::string &filename) const;
        /**
         * @brief Charge une Image depuis un fichier de nom filename.
         * 
         * @param filename Nom du fichier d'où est chargée l'Image.
        */
        void ouvrir(const std::string &filename);
        /**
         * @brief Affiche l'Image dans la console, composante par composante.
        */
        void afficherConsole() const;
        static void testRegression();
};

#endif