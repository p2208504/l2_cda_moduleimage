#include "Pixel.h"

Pixel::Pixel() {r = 0; g = 0; b = 0;}

Pixel::Pixel(int rr, int gg, int bb) {
    r = (unsigned char) rr;
    g = (unsigned char) gg;
    b = (unsigned char) bb;
}