#include "Image.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

Image::Image() {
    dimx = 0;
    dimy = 0;
    tab = nullptr;
}

Image::Image(int widht, int height) {
    dimx = widht;
    dimy = height;
    tab = new Pixel[dimx*dimy];
}

Image::~Image() {
    if (not (tab == nullptr))
        delete [] tab;
    tab = nullptr;
    dimx = 0;
    dimy = 0;
}

void verifieCoordonnes(int x, int y, int dimx, int dimy) {
    if (0 <= x && x < dimx && 0 <= y && y < dimy)
        return;
    else {
        std::cout<<"Coordonees incorrectes: "<<x<<" "<<y<<" "<<dimx<<" "<<dimy<<std::endl;
        exit(EXIT_FAILURE);
    }
}

Pixel& Image::getPix(int x, int y) {
    verifieCoordonnes(x, y, dimx, dimy);
    return tab[dimx*y+x];
}

Pixel Image::getPixCopie(int x, int y) const {
    verifieCoordonnes(x, y, dimx, dimy);
    Pixel pix;
    pix.b = tab[dimx*y+x].b;
    pix.g = tab[dimx*y+x].g;
    pix.r = tab[dimx*y+x].r;
    return pix;
}

void Image::setPix(int x, int y, Pixel couleur) {
    verifieCoordonnes(x, y, dimx, dimy);
    tab[dimx*y+x].b = couleur.b;
    tab[dimx*y+x].g = couleur.g;
    tab[dimx*y+x].r = couleur.r;
}

void Image::dessinerRectangle(int xmin, int ymin, int xmax, int ymax, Pixel couleur) {
    verifieCoordonnes(xmin, ymin, xmax+1, ymax+1);
    verifieCoordonnes(xmax, ymax, dimx, dimy);
    for (int i = xmin; i<=xmax; i++) {
        for (int j = ymin; j<=ymax; j++) {
            setPix(i, j, couleur);
        }
    }
}

void Image::effacer(Pixel couleur) {
    dessinerRectangle(0, 0, dimx-1, dimy-1, couleur);
}

void Image::sauver(const std::string &filename) const {
    std::ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << std::endl;
    fichier << dimx << " " << dimy << std::endl;
    fichier << "255" << std::endl;
    Pixel pix;
    for (unsigned int y = 0; y < (unsigned int) dimy; y++)
    {
        for (unsigned int x = 0; x < (unsigned int) dimx; x++)
        {
            pix = getPixCopie(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        fichier << std::endl;
    }
    std::cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const std::string &filename) {
    std::ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    int r, g, b;
    std::string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    Pixel pix;
    for (unsigned int y = 0; y < (unsigned int) dimy; y++)
        for (unsigned int x = 0; x < (unsigned int) dimx; x++)
        {
            fichier >> r >> g >> b;
            pix.r = r;
            pix.g = g;
            pix.b = b;
            setPix(x, y, pix);
        }
    fichier.close();
    std::cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole() const {
    std::cout << dimx << " " << dimy << std::endl;
    Pixel pix;
    for (unsigned int y = 0; y < (unsigned int) dimy; ++y)
    {
        for (unsigned int x = 0; x < (unsigned int) dimx; ++x)
        {
            pix = getPixCopie(x, y);
            std::cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        std::cout << std::endl;
    }
}

void Image::testRegression() {
    Image* I1 = new Image;
    Image I2(12, 14);
    assert(I2.dimx == 12);
    assert(I2.dimy == 14);
    std::cout<<(int) I2.tab[(12*14)-1].b<<std::endl;
    Pixel pix = I2.getPix(4, 3);
    assert(pix.r == 0);
    assert(pix.g == 0);
    assert(pix.b == 0);
    I2.setPix(8, 4, pix);
    Pixel testpix = I2.getPixCopie(6, 6);
    assert(testpix.r == 0);
    Pixel couleur(10, 20, 30);
    I2.dessinerRectangle(5, 2, 7, 10, couleur);
    testpix = I2.getPixCopie(6, 6); 
    assert(testpix.b == 20);
    couleur.b = 50;
    I2.effacer(couleur);
    testpix = I2.getPixCopie(6, 6);
    assert(testpix.b == 60);
    delete I1;
    Pixel rouge (205, 9, 13);
    Pixel jaune (242, 248, 22);
    Pixel bleu (120, 193, 246);
    Image imTest(3, 2);
    imTest.afficherConsole();
    imTest.effacer(bleu);
    imTest.afficherConsole();
    imTest.dessinerRectangle(0, 0, 1, 0, jaune);
    imTest.afficherConsole();
    imTest.sauver("./data/imTest.ppm");
    imTest.ouvrir("./data/imTest.ppm");
    imTest.afficherConsole();
}
