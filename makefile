all: bin/affichage bin/exemple bin/test

bin/affichage: obj/affichage.o obj/Pixel.o obj/Image.o obj/ImageViewer.o
	g++ -Wall obj/affichage.o obj/Pixel.o obj/Image.o obj/ImageViewer.o -lSDL2 -lSDL2_ttf -lSDL2_image -o bin/affichage

obj/affichage.o: src/mainAffichage.cpp src/Pixel.h src/Image.h src/ImageViewer.h
	g++ -Wall -c src/mainAffichage.cpp -o obj/affichage.o

bin/exemple: obj/exemple.o obj/Pixel.o obj/Image.o
	g++ -Wall obj/exemple.o obj/Pixel.o obj/Image.o -o bin/exemple

obj/exemple.o: src/mainExemple.cpp src/Pixel.h src/Image.h
	g++ -Wall -c src/mainExemple.cpp -o obj/exemple.o

bin/test: obj/test.o obj/Pixel.o obj/Image.o
	g++ -Wall obj/test.o obj/Pixel.o obj/Image.o -o bin/test

obj/test.o: src/mainTest.cpp src/Pixel.h src/Image.h
	g++ -Wall -c src/mainTest.cpp -o obj/test.o

obj/Pixel.o: src/Pixel.cpp src/Pixel.h
	g++ -Wall -c src/Pixel.cpp -o obj/Pixel.o

obj/Image.o: src/Image.cpp src/Image.h
	g++ -Wall -c src/Image.cpp -o obj/Image.o

obj/ImageViewer.o: src/ImageViewer.cpp src/ImageViewer.h
	g++ -Wall -c src/ImageViewer.cpp -o obj/ImageViewer.o

clean:
	rm bin/* ; rm obj/*

veryclean: clean
	rm data/imAff.ppm ; rm data/imTest.ppm