#ifndef _PIXEL_H
#define _PIXEL_H

/**
 * @struct Pixel
 * 
 * @brief Un Pixel est représenté par ses composantes en rouge, vert et bleu.
*/

struct Pixel {
    unsigned char r; ///<Composante rouge, de 0 à 255.
    unsigned char g; ///<Composante verte, de 0 à 255.
    unsigned char b; ///<Composante bleue, de 0 à 255.

    /**
     * @brief Constructeur par défaut de Pixel. Donne un Pixel noir.
    */
    Pixel();
    /**
     * @brief Constructeur par initialisation de Pixel.
     * 
     * @param rr Composante rouge du Pixel.
     * @param gg Composante verte du Pixel.
     * @param bb Composante bleue du Pixel
    */
    Pixel(int rr, int gg, int bb);
};

#endif