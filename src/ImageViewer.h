#ifndef _IMAGEVIEWER_H
#define _IMAGEVIEWER_H

#include <SDL2/SDL.h>
#include "Image.h"

/**
 * @class ImageViewer
 * 
 * @brief Un ImageViewer utilise la SDL pour pouvoir charger des Image et les afficher dans des fenêtres.
 * @todo La fenêtre s'ouvre mais l'Image n'apparaît pas encore.
*/

class ImageViewer {
    private:
        SDL_Window* window;
        SDL_Renderer* renderer;
        SDL_Surface* surface;
        SDL_Texture* texture;

    public:
        /**
         * @brief Constructeur de ImageViewer. Initialise la SDL.
        */
        ImageViewer();
        /**
         * @brief Destructeur de ImageViewer. Quitte la SDL.
        */
        ~ImageViewer();

        /**
         * @brief Charge l'Image depuis un fichier.
         * 
         * @param filename Nom du fichier où est stockée l'Image.
        */
        void loadFromFile(const char* filename);
        /**
         * @brief Affiche l'image chargée dans la fenêtre.
         * 
         * @param x Abscisse où est affichée l'Image.
         * @param y Ordonnée où est affichée l'Image.
         * @param w Largeur avec laquelle sera affichée l'Image.
         * @param h Hauteur avec laquelle sera affichée l'Image.
        */
        void imAff(int x, int y, int w, int h);
        /**
         * @brief Boucle d'affichage de l'Image dans une fenêtre. On peut quitter en appuyant sur "Q".
         * 
         * @param im Image qui sera affichée dans la fenêtre.
        */
        void afficher(const Image& im);
};

#endif